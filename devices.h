/* devices.h
 *
 * (c) 2015, 2018 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#ifndef DEVICES_H
#define DEVICES_H

int get_device_id(const int port, const char *device_file, 
		  const int portnumber, char *device_id);
#endif
