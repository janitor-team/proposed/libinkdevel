/* libusb-utils.c
 *
 * (c) 2014, 2015 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#include "config.h"

#include "libusb-utils.h"

#include <libusb.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/* 
 * Initializes libusb.
 *
 * Returns USB_SUCCESS or USB_FAILURE.
 */
int init_usb(libusb_context *ctx) {
  int result;

  result = libusb_init(&ctx); //initialize the library for the session
  
  if (result < LIBUSB_SUCCESS) {
    
#ifdef DEBUG

    printf("Init Error: %d\n", result);

#endif

    return USB_FAILURE;
  }

#ifdef DEBUG

#if (HOST_OS == LINUX)

  libusb_set_debug(ctx, 4); //set verbosity level to 4

#elif (HOST_OS == FREEBSD)

  libusb_set_debug(ctx, 1); //set verbosity level to 1

#endif

#endif

  return USB_SUCCESS;
}

/*
 * Deinitializes libusb.
 */
void shutdown_usb(libusb_context *ctx) {
  libusb_exit(ctx); //needs to be called to end the session
}

/*
 * Finds an USB printer and returns a struct describing it. If no printer is 
 * found NULL is returned.
 */
usb_printer *find_printer(libusb_context *ctx, int instance) {
  libusb_device **devs; //pointer to pointer of device, 
                        //used to retrieve a list of devices
  libusb_device_handle *dev_handle; //a device handle
  ssize_t cnt; //holding number of devices in list
  int i; // loop counter
  int printers_found = 0; // used for finding the correct printer
  usb_printer *printer = NULL;

  cnt = libusb_get_device_list(ctx, &devs); //get the list of devices

  if (cnt < LIBUSB_SUCCESS) {

#ifdef DEBUG

    printf( "Get device error\n");

#endif

    return NULL;
  }

#ifdef DEBUG

  printf("%d devices in list\n", (int) cnt);

#endif

  for (i = 0; i < cnt; i++) {
    if (libusb_open(devs[i], &dev_handle)) {

#ifdef DEBUG

      printf("Could not open device\n");

#endif

    } else {
      printer = check_for_printer(dev_handle);

      if (printer != NULL) {
	printers_found++;

	if (printers_found == instance + 1) {
	  printer->device = libusb_ref_device(devs[i]);
	  libusb_close(dev_handle);
	  break;
	}
      }

      libusb_close(dev_handle);
    }
  }

#ifdef DEBUG

  if (printers_found == 0) {
    printf("Cannot find printer\n");
  } else {
    printf("Printers found: %d, requested: %d\n", printers_found, instance + 1);
  }

#endif

  libusb_free_device_list(devs, 1); //free the list, unref the devices in it

  if (printers_found == instance + 1) {
    return printer;
  } else {
    return NULL;
  }
}

/*
 * Releases a device handle. Returns USB_SUCCESS or USB_FAILURE.
 */
int release_device_handle(usb_printer *printer) {
  int r;

  r = libusb_release_interface(printer->handle, printer->interface); 
  
  if (r != LIBUSB_SUCCESS) {

#ifdef DEBUG

    printf("Cannot release interface\n");

#endif

    return USB_FAILURE;
  }

#ifdef DEBUG

  printf("Released interface %d\n", printer->interface);

#endif

#if (HOST_OS == FREEBSD)

  /*
  if (!libusb_attach_kernel_driver(printer->handle, printer->interface)) {

#ifdef DEBUG

    printf("Cannot reattach kernel driver\n");

#endif

  }
  */
#endif
  
  
  libusb_close(printer->handle); //close the device we opened
  libusb_unref_device(printer->device);

#ifdef DEBUG

  printf("Device closed\n");

#endif
  
  return USB_SUCCESS;
}

/*
 * Retrieves the IEEE1284 device id. Returns USB_SUCCESS or USB_FAILURE.
 */
int get_usb_device_id(usb_printer *printer, char *buffer, size_t bufsize) {

  int length; //Length of device ID

  if (libusb_control_transfer(printer->handle,
			      LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_ENDPOINT_IN |
			      LIBUSB_RECIPIENT_INTERFACE,
			      0, printer->configuration, 
                              (printer->interface << 8) | printer->altsetting, 
			      (unsigned char *)buffer, bufsize, 5000) < 0)
    {
      *buffer = '\0';
      return USB_FAILURE;
    }

  /*
   * Extract the length of the device ID string from the first two
   * bytes.  The 1284 spec says the length is stored MSB first...
   */

  length = (((unsigned)buffer[0] & 255) << 8) |
    ((unsigned)buffer[1] & 255);

  /*
   * Check to see if the length is larger than our buffer or less than 14 bytes
   * (the minimum valid device ID is "MFG:x;MDL:y;" with 2 bytes for the 
   * length).
   *
   * If the length is out-of-range, assume that the vendor incorrectly
   * implemented the 1284 spec and re-read the length as LSB first,..
   */

  if (length > bufsize || length < 14)
    length = (((unsigned)buffer[1] & 255) << 8) |
      ((unsigned)buffer[0] & 255);

  if (length > bufsize)
    length = bufsize;

  if (length < 14)
    {
      /*
       * Invalid device ID, clear it!
       */

      *buffer = '\0';
      return USB_FAILURE;
    }

  length -= 2;

  /*
   * Copy the device ID text to the beginning of the buffer and
   * nul-terminate.
   */

  memmove(buffer, buffer + 2, length);
  buffer[length] = '\0';

  return USB_SUCCESS;
}

/*
 * Checks if the given handle belongs to a printer. If true, communication 
 * paramters for the printer will be determined and returned in a struct.
 * Otherwise NULL we be returned.
 */
usb_printer *check_for_printer(libusb_device_handle *handle) {
  libusb_device* device;
  struct libusb_device_descriptor devdesc; //Current device descriptor
  struct libusb_config_descriptor *confptr = NULL; //Pointer to current 
                                                   //configuration
  const struct libusb_interface *ifaceptr = NULL; //Pointer to current interface
  const struct libusb_interface_descriptor *altptr = NULL; //Pointer to current
                                                           //alternate setting
  const struct libusb_endpoint_descriptor *endpptr = NULL; //Pointer to current
                                                           //endpoint
  uint8_t conf,	         //Current configuration
    iface,	         //Current interface
    altset,	         //Current alternate setting
    protocol,	         //Current protocol
    endp,	         //Current endpoint
    selected_altset = 0, //Selected altset
    printer_found = 0,   //Printer found?
    read_endp,           //Read endpoint
    write_endp,          //Write endpoint
    interface = 0,       //Interface
    configuration;       //Configuration
  usb_printer *printer = NULL;

  device = libusb_get_device(handle);

  /*
   * Ignore devices with no configuration data and anything that is not
   * a printer...
   */

  if (libusb_get_device_descriptor(device, &devdesc) < LIBUSB_SUCCESS) {
    return NULL;
  }

  if (!devdesc.bNumConfigurations || !devdesc.idVendor || !devdesc.idProduct) {
    return NULL;
  }

  for (conf = 0; conf < devdesc.bNumConfigurations && !printer_found; conf++) {

    if (libusb_get_config_descriptor(device, conf, &confptr) < LIBUSB_SUCCESS) {
      return NULL;
    }

#ifdef DEBUG

    printf("configuration %d out of %d configurations\n", conf, 
           devdesc.bNumConfigurations);

#endif

    for (iface = 0; iface < confptr->bNumInterfaces && !printer_found; 
         iface++) {

      ifaceptr = &confptr->interface[iface];

#ifdef DEBUG

      printf("interface %d out of %d interfaces\n", iface, 
	     confptr->bNumInterfaces);

#endif
	
      /*
       * Some printers offer multiple interfaces...
       */

      protocol = 0;
      
      for (altset = 0; altset < ifaceptr->num_altsetting && !printer_found; 
           altset++) {

	altptr = &ifaceptr->altsetting[altset];

#ifdef DEBUG

	printf("altset %d out of %d altsets\n", altset, 
	       ifaceptr->num_altsetting);

#endif

	assert(altptr != NULL);

	/*
	 * Currently we only support unidirectional and bidirectional
	 * printers.  Future versions of this code will support the
	 * 1284.4 (packet mode) protocol as well.
	 */
	
#ifdef DEBUG

	printf ("interface class: %d\ninterface subclass: %d\n", 
		altptr->bInterfaceClass, altptr->bInterfaceSubClass);
	printf("interface protocol: %d\n",  altptr->bInterfaceProtocol);

#endif

	if (((altptr->bInterfaceClass != LIBUSB_CLASS_PRINTER ||
	      altptr->bInterfaceSubClass != 1)) ||
	    (altptr->bInterfaceProtocol != 1 &&	/* Unidirectional */
	     altptr->bInterfaceProtocol != 2) ||	/* Bidirectional */
	    altptr->bInterfaceProtocol < protocol) {
	  continue;
	}

	read_endp  = -1;
	write_endp = -1;
	
	for (endp = 0; endp < altptr->bNumEndpoints; endp++) {
	  
	  endpptr = &altptr->endpoint[endp];

	  if ((endpptr->bmAttributes & LIBUSB_TRANSFER_TYPE_MASK) ==
	      LIBUSB_TRANSFER_TYPE_BULK) {
	    if (endpptr->bEndpointAddress & LIBUSB_ENDPOINT_DIR_MASK) {
	      read_endp = endpptr->bEndpointAddress; 
	    } else {
	      write_endp = endpptr->bEndpointAddress;
	    }

	    interface = altptr->bInterfaceNumber;
	    selected_altset = altptr->bAlternateSetting;
	    printer_found = 1;
	  }
	}

	if (printer_found) {
	  /*
	   * Save the best match so far...
	   */
		  
	  protocol = altptr->bInterfaceProtocol;
          configuration = confptr->bConfigurationValue;

#ifdef DEBUG

	  printf("write endpoint is %d\n", write_endp);

	  if (protocol > 1)
	    printf("read endpoint is %d\n", read_endp);
          
          printf("configuration selected for communication: %d\n", 
                 configuration);
	  printf("interface selected for communication: %d\n", interface);
	  printf("alt setting selected for communication: %d\n", 
		 selected_altset);

#endif

	  printer = (usb_printer *) malloc(sizeof(usb_printer));
	  
	  if (printer != NULL) {
	    printer->handle = handle;
	    printer->read_endp = read_endp;
	    printer->write_endp = write_endp;
	    printer->interface = interface;
	    printer->altsetting = selected_altset;
            printer->configuration = configuration;
	  }

	  break;

	}
      }
    }

    libusb_free_config_descriptor(confptr);

  }

  return printer;
}

/*
 * Opens the given printer. Then the according interface is claimed and the 
 * appropiate configuration is selected. 
 * 
 * Returns USB_SUCCESS or USB_FAILURE.
 */
int open_device_handle(usb_printer *printer) {
  int r;

  if (printer != NULL && printer->device != NULL) {
    if (libusb_open(printer->device, &printer->handle) == LIBUSB_SUCCESS) {

#ifdef DEBUG

      printf("Device opened\n");

#endif

#if (HOST_OS == LINUX)

      if (libusb_set_auto_detach_kernel_driver(printer->handle, 1) != 
	  LIBUSB_SUCCESS) {

#ifdef DEBUG

	printf("Could not set auto detach for kernel driver\n");

#endif

      }

#elif (HOST_OS == FREEBSD)
      /*
      if (libusb_kernel_driver_active(printer->handle, printer->interface)) {
        if (libusb_detach_kernel_driver(printer->handle, 
                                           printer->interface) 
            != LIBUSB_SUCCESS) {

#ifdef DEBUG

          printf("Could not detach kernel driver\n");

#endif

        }
      }
      */
#endif

#ifdef DEBUG

      printf("Trying to set configuration %d\n", printer->configuration);

#endif

      assert(printer->handle != NULL);

      r = libusb_set_configuration(printer->handle, printer->configuration);

      if (r < LIBUSB_SUCCESS) {

#ifdef DEBUG

	printf("Cannot set configuration: error %d\n", r);

#endif

      }

#ifdef DEBUG

      printf("Trying to claim interface %d\n", printer->interface);

#endif
      r = libusb_claim_interface(printer->handle, printer->interface);

      if (r < LIBUSB_SUCCESS) {

#ifdef DEBUG

	printf("Cannot claim interface: error %d\n", r);

#endif

#if (HOST_OS == FREEBSD)

        /*
        if (!libusb_attach_kernel_driver(printer->handle, printer->interface)) {

#ifdef DEBUG

          printf("Cannot reattach kernel driver\n");

#endif

        }
        */

#endif

	libusb_close(printer->handle);
	
#ifdef DEBUG

	printf("Device closed\n");

#endif

	return USB_FAILURE;
      }
      
#ifdef DEBUG

      printf("Claimed interface %d\n", printer->interface);

#endif
  
      r = libusb_set_interface_alt_setting(printer->handle, printer->interface, 
					 printer->altsetting);

      if (r < LIBUSB_SUCCESS) {

#ifdef DEBUG

	printf("Cannot activate alternate setting: error %d\n", r);

#endif

	r = libusb_release_interface(printer->handle, printer->interface);

#ifdef DEBUG

	if (r < LIBUSB_SUCCESS) {
	  printf("Could not release interface %d: error %d\n", 
		 printer->interface, r);
	}

#endif
	
#if (HOST_OS == FREEBSD)

        /*
        if (!libusb_attach_kernel_driver(printer->handle, printer->interface)) {

#ifdef DEBUG

          printf("Cannot reattach kernel driver\n");
          
#endif

        }
        */

#endif

	libusb_close(printer->handle);

#ifdef DEBUG

	printf("Device closed\n");

#endif

	return USB_FAILURE;
      }
      
#ifdef DEBUG

      printf("Activated alternate setting %d for interface %d\n", 
	     printer->altsetting, printer->interface);

#endif
    
      return USB_SUCCESS;
    } else {
     
#ifdef DEBUG

      printf("Could not open printer\n");

#endif

      return USB_FAILURE;
    }  
  } else {
    
#ifdef DEBUG

    printf ("printer is null or device is null\n");

#endif

    return USB_FAILURE;    

  }
}

/*
 * Transfers data to or from a device. The number of transfered bytes will be
 * stored in transfered. The direction is determined by the endpoint.
 *
 *  Returns USB_SUCCESS or USB_FAILURE.
 */
int bulk_transfer(libusb_device_handle *handle, uint8_t endp,
		  char *buffer, size_t bufsize, int *transfered) {

  int result = libusb_bulk_transfer (handle, endp, (unsigned char *) buffer, 
				     bufsize, transfered, USB_TIMEOUT);
    if (result == LIBUSB_SUCCESS) {
      return USB_SUCCESS;
    } else {

#ifdef DEBUG

      printf("Data transfer failed: %d\n", result);

#endif

      return USB_FAILURE;
    }
}
