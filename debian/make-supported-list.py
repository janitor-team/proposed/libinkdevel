#!/usr/bin/python2
# -*- coding: utf-8 -*-

import urllib2
import re
from time import localtime, strftime

htmloutput = urllib2.urlopen("http://libinklevel.sourceforge.net/index.html").read()
htmloutputlist = htmloutput.split("\n")

i = 0
startsup = None
for line in htmloutputlist:
	if re.match(r'^<h2>Printers supported</h2>$',line.strip()):
		startsup = i + 5
	if re.match(r'^</ul>',line.strip()) and startsup != None:
		endsup = i
		break
	i = i + 1

j = 0
startunsup = None
for line in htmloutputlist:
	if re.match(r'<h2>Printers not supported</h2>$',line.strip()):
		startunsup = j + 5
	if re.match(r'^</ul>',line.strip()) and startunsup != None:
		endunsup = j
		break
	j = j + 1

print "libinklevel for Debian\n----------------------\n"

print "Supported printer:"
for line in htmloutputlist[startsup:endsup]:
	print "  "+line.strip().strip('<li>').strip('</li>')

print "\nUnsupported printer:"
for line in htmloutputlist[startunsup:endunsup]:
	print "  "+line.strip().strip('<li>').strip('</li>')

print "\nA note about Canon printers:"
print "  Some Canon printers transmit their ink levels as a binary value."
print "  They report only that there is enough ink or not enough ink for a"
print "  particular cartridge. Libinklevel then reports 100% or 20% "
print "  respectively."

print "\nA note about Epson printers:"
print "  Under some circumstances, especially when one ink catridge is"
print "  completely empty, it may take about 20 to 30 seconds to detect the ink"
print "  level. Please take this into account before submitting a bug report."
print "  When you query the ink levels before the printer is fully initialized"
print "  a sheet of paper will be drawn in. But nothing will be printed. To avoid"
print "  this do not query the ink levels before the printer is fully initialized."

print "\n -- Adam Cécile (Le_Vert) <gandalf@le-vert.net>, "+strftime("%a, %d %b %Y %H:%M:%S +0001", localtime())
