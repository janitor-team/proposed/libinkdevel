/* d4lib.h
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef D4LIB_H
#define D4LIB_H

#include "printer-io.h"

extern int debugD4;   /* allow printout of debug informations */

extern int EnterIEEE(printer_descriptor *desc);
extern int Init(printer_descriptor *desc);
extern int Exit(printer_descriptor *desc);
extern int GetSocketID(printer_descriptor *desc, const char *serviceName);
extern int OpenChannel(printer_descriptor *desc, unsigned char sockId, 
		       int *sndSz, int *rcvSz);
extern int CloseChannel(printer_descriptor *desc, unsigned char socketID);
extern int CreditRequest(printer_descriptor *desc, unsigned char socketID);
extern int Credit(printer_descriptor *desc, unsigned char socketID, int credit);

/* convenience function */
extern int SafeWrite(printer_descriptor *desc, void *data, int len);
extern int askForCredit(printer_descriptor *desc, unsigned char socketID, 
			int *sndSz, int *rcvSz);
extern int writeData(printer_descriptor *desc, unsigned char socketID, 
		     const unsigned char *buf, int len, int eoj);
extern int readData(printer_descriptor *desc, unsigned char socketID, 
		    unsigned char *buf, int len);
extern int readAnswer(printer_descriptor *desc, unsigned char *buf, int len);
extern void flushData(printer_descriptor *desc, unsigned char socketID);
extern void setDebug(int debug);

extern int d4WrTimeout;
extern int d4RdTimeout;
extern int ppid;

#ifdef D4_DEBUG
#define DEBUG 1
#endif

#endif
